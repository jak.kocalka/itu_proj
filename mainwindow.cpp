/*
 * Created on Fri Dec 02 2019
 *
 * Copyright (c) 2019 Jakub Kočalka
 * @file model.cpp
 * @author Jakub Kocalka, xkocal00; Juraj Sloboda, xslobo07; Jiri Zilka, xzilka11
 * @brief Controller
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QKeyEvent>
#include <QMediaPlayer>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;

    //connect(ui->StartButton, SIGNAL(released()), this, SLOT(Startinggame()));
    //connect(ui->PlayAgain, SIGNAL(released()), this, SLOT(Startinggame()));


    //connect(ui->Button1, SIGNAL(released()), this, SLOT(PressingButton()));
    //connect(ui->Button2, SIGNAL(released()), this, SLOT(PressingButton()));
    //connect(ui->Button3, SIGNAL(released()), this, SLOT(PressingButton()));
    //connect(ui->Button4, SIGNAL(released()), this, SLOT(PressingButton()));

}

void MainWindow::Startinggame(){
    //pustenie jedného zvuku a nastavenie premennej na danú vec, spustenie timeru?
    ui->CorrectAnwersStart->setText("Správnych odpovedí: 0");
    ui->IncorrectAnswersStart->setText("Správnych odpovedí: 0" );

    GameCounter = 150;
    div_t time = div (GameCounter,60);
    ui->TimeLine->setText("Zbývajúci čas: " + QString::number(time.quot) + ":" + QString::number(time.rem));
    Correct = 0;
    Incorrect = 0;
    Started = true;

    QTimer * GameTimeCounter = new QTimer();
    GameTimeCounter->setSingleShot(true);
    QObject::connect(GameTimeCounter,SIGNAL(timeout()),this,SLOT(MyCounter()));
    GameTimeCounter->start(1000);

    int i = rand() % 3000 + 2500;
    QTimer * timer = new QTimer();
    timer->setSingleShot(true);
    QObject::connect(timer,SIGNAL(timeout()),this,SLOT(getsound()));
    timer->start(i);


}

void MainWindow::MyCounter(){
    if(Started){
        if (GameCounter ==  0){
            Endgame();
        }else if (GameCounter > 0){
            QTimer * GameTimeCounter = new QTimer();
            GameTimeCounter->setSingleShot(true);
            QObject::connect(GameTimeCounter,SIGNAL(timeout()),this,SLOT(MyCounter()));
            GameTimeCounter->start(1000);
            GameCounter--;
        }
        div_t time = div (GameCounter,60);
        ui->TimeLine->setText("Zbývajúci čas: " + QString::number(time.quot) + ":" + QString::number(time.rem));
    }

}



void MainWindow::keyPressEvent(QKeyEvent *event){
    if (CanPress){
        if (Started){
            if (event->key() == Qt::Key_A){
                if (ExpectingSound == 1){
                    model.userAnswered(true);
                    ui->CorrectAnwersStart->setText("Správnych odpovedí: " + QString::number(model.getCorrectAnswers()));

                    CanPress = false;
                }else{model.userAnswered(false);
                    ui->IncorrectAnswersStart->setText("Neprávnych odpovedí: " + QString::number(model.getIncorrectAnswers()));


                }
            }else if (event->key() == Qt::Key_S){
                if (ExpectingSound == 2){
                    model.userAnswered(true);
                    ui->CorrectAnwersStart->setText("Správnych odpovedí: " + QString::number(model.getCorrectAnswers()));

                    CanPress = false;
                }else{model.userAnswered(false);
                    ui->IncorrectAnswersStart->setText("Neprávnych odpovedí: " + QString::number(model.getIncorrectAnswers()));


                }
            }else if (event->key() == Qt::Key_K){
                if (ExpectingSound == 3){
                    model.userAnswered(true);
                    ui->CorrectAnwersStart->setText("Správnych odpovedí: " + QString::number(model.getCorrectAnswers()));

                    CanPress = false;
                }else{model.userAnswered(false);
                    ui->IncorrectAnswersStart->setText("Neprávnych odpovedí: " + QString::number(model.getIncorrectAnswers()));


                }
            }else if (event->key() == Qt::Key_L){
                if (ExpectingSound == 4){model.userAnswered(true);
                    ui->CorrectAnwersStart->setText("Správnych odpovedí: " + QString::number(model.getCorrectAnswers()));

                    CanPress = false;
                }else{model.userAnswered(false);
                    ui->IncorrectAnswersStart->setText("Neprávnych odpovedí: " + QString::number(model.getIncorrectAnswers()));


                }
            }
        }


    }else {
        model.userAnswered(false);
        ui->IncorrectAnswersStart->setText("Neprávnych odpovedí: " + QString::number(model.getIncorrectAnswers()));

    }




}


void MainWindow::Endgame(){

    if(Started){
        ui->stackedWidget->setCurrentIndex(3);
        Started = false;
        int together = Incorrect + Correct;
        ui->IncorrectAnswersLine->setText(QString::number(model.getIncorrectAnswers()) + "/" + QString::number(model.getTotalAnswers()) );
        ui->CorrectAnswersLine->setText(QString::number(model.getCorrectAnswers()) + "/" + QString::number(model.getTotalAnswers()));
        double Percent = Correct/(double)together * 100;
        ui->PercentLine->setText(QString::number(Percent,'f',2) + "%");
        Started = false;
        Correct = 0;
        Incorrect = 0;
        ExpectingSound = 0;
        CanPress = false;
        //GameTimeCounter->stop();
    }
   // ui->Incorre->setText("incorrect");

}

void MainWindow::getsound(){

    if(Started){
        QMediaPlayer *musicplayer = new QMediaPlayer();
        ExpectingSound = rand() % 3 + 1;

        switch (ExpectingSound) {
        case 1:
            musicplayer->setMedia(QUrl("qrc:/Sounds/sounds/Cow.mp3"));
            break;
        case 2:
            musicplayer->setMedia(QUrl("qrc:/Sounds/sounds/Frog.mp3"));
            break;
        case 3:
            musicplayer->setMedia(QUrl("qrc:/Sounds/sounds/Duck.mp3"));
            break;
        default:
            break;
        }

        CanPress = true;
        if (musicplayer->state() == QMediaPlayer::PlayingState){
              musicplayer->setPosition(0);
        }
        else if (musicplayer->state() == QMediaPlayer::StoppedState){

              musicplayer->play();
        }
    }

}

void MainWindow::PressingButton(int ButtonPressed){
    //pustenie jedného zvuku a nastavenie premennej na danú vec, spustenie timeru?
    if (CanPress){
        if (ButtonPressed == ExpectingSound){
            model.userAnswered(true);
            ui->CorrectAnwersStart->setText("Správnych odpovedí: " + QString::number(model.getCorrectAnswers()));
            CanPress = false;

            int i = rand() % 3000 + 2500;
            QTimer * timer = new QTimer();
            timer->setSingleShot(true);
            QObject::connect(timer,SIGNAL(timeout()),this,SLOT(getsound()));
            timer->start(i);
        }else {model.userAnswered(false);
            ui->IncorrectAnswersStart->setText("Neprávnych odpovedí: " + QString::number(model.getIncorrectAnswers()));

        }


    }else {model.userAnswered(false);
        ui->IncorrectAnswersStart->setText("Neprávnych odpovedí: " + QString::number(model.getIncorrectAnswers()));

    }

}

void MainWindow::on_StartButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    Startinggame();
}


void MainWindow::on_BackFromSettings_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_QuitButton_clicked()
{
    Endgame();
}

void MainWindow::on_MenuFromQuit_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_PlayAgain_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    Startinggame();
}

void MainWindow::on_Button1_clicked()
{
    PressingButton(1);
}

void MainWindow::on_Button2_clicked()
{
    PressingButton(2);
}
void MainWindow::on_Button3_clicked()
{
    PressingButton(3);
}
void MainWindow::on_Button4_clicked()
{
    PressingButton(4);
}


void MainWindow::on_SettingsButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}
