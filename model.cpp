/*
 * Created on Fri Dec 02 2019
 *
 * Copyright (c) 2019 Jakub Kočalka
 * @file model.cpp
 * @author Jakub Kocalka, xkocal00; Juraj Sloboda, xslobo07; Jiri Zilka, xzilka11
 * @brief Data model
 */

#include "model.h"

model::model()
{
    correctAnswers = 0;
    incorrectAnswers = 0;
}

int model::getCorrectAnswers(){
    return correctAnswers;
}

int model::getIncorrectAnswers(){
    return incorrectAnswers;
}

int model::getTotalAnswers(){
    return correctAnswers + incorrectAnswers;
}

void model::userAnswered(bool correct){
    if(correct){
        setCorrectAnswers(getCorrectAnswers()+1);
    }else{
        setIncorrectAnswers(getIncorrectAnswers()+1);
    }
}

int model::setCorrectAnswers(int value){
    correctAnswers = value;
}
int model::setIncorrectAnswers(int value){
    incorrectAnswers = value;
}
