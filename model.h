/*
 * Created on Fri Dec 02 2019
 *
 * Copyright (c) 2019 Jakub Kočalka
 * @file model.cpp
 * @author Jakub Kocalka, xkocal00; Juraj Sloboda, xslobo07; Jiri Zilka, xzilka11
 * @brief Data model
 */

#ifndef MODEL_H
#define MODEL_H


class model
{
public:
    model();

    int getCorrectAnswers();
    int getIncorrectAnswers();
    int getTotalAnswers();

    void userAnswered(bool correct);

private:
    int correctAnswers;
    int incorrectAnswers;

    int setCorrectAnswers(int value);
    int setIncorrectAnswers(int value);
};

#endif // MODEL_H
