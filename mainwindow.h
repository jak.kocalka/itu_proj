/*
 * Created on Fri Dec 02 2019
 *
 * Copyright (c) 2019 Jakub Kočalka
 * @file model.cpp
 * @author Jakub Kocalka, xkocal00; Juraj Sloboda, xslobo07; Jiri Zilka, xzilka11
 * @brief Controller
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <string.h>
#include <QTimer>

#include "model.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_StartButton_clicked();
    void on_BackFromSettings_clicked();

    void on_SettingsButton_clicked();
    void on_QuitButton_clicked();
    void on_MenuFromQuit_clicked();
    void on_PlayAgain_clicked();
    void MyCounter();
    void Startinggame();
    void PressingButton(int ButtonPressed);
    void getsound();
    void keyPressEvent(QKeyEvent *event);
    void Endgame();
    void on_Button1_clicked();
    void on_Button2_clicked();
    void on_Button3_clicked();
    void on_Button4_clicked();

private:
    Ui::MainWindow *ui;
    bool Started = false;
    model model;
    int Correct = 0;
    int Incorrect = 0;
    int ExpectingSound = 0;
    bool CanPress = false;
    int GameCounter = 0;
    //QTimer * GameTimeCounter = new QTimer();
};

#endif // MAINWINDOW_H
